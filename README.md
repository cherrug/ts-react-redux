# Simple implementation of todo list using TypeScript above React+Redux

This project has been created for basic understanding TS features for React+Redux app.

This project has been generated with help of `create-react-app`.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.

### `yarn build`

Builds the app for production to the `build` folder.
