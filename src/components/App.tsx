import React from 'react';
import { connect } from 'react-redux';
import { Todo, fetchTodos, deleteTodo } from '../actions';
import { StoreState } from '../reducers';

interface AppProps {
    todos: Todo[];
    fetchTodos: Function;
    deleteTodo: typeof deleteTodo;
}

interface AppState {
    loading: boolean;
}

class _App extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);

        this.state = { loading: false };
    }

    componentDidUpdate(prevProps: AppProps) {
        if (!prevProps.todos.length && this.props.todos.length) {
            this.setState({ loading: false });
        }
    }

    onFetchClick = (): void => {
        this.setState({ loading: true });
        this.props.fetchTodos();
    };

    onTodoClick = (id: number) => {
        this.props.deleteTodo(id);
    };

    renderTodoList(): JSX.Element[] {
        return this.props.todos.map((todo: Todo) => (
            <div onClick={() => this.onTodoClick(todo.id)} key={todo.id}>
                {todo.title}
            </div>
        ));
    }

    render() {
        return (
            <div>
                <h3>Bloody Jesus! This is a todo list!</h3>
                <button onClick={this.onFetchClick}>Fetch 'em</button>
                <div>{this.state.loading ? 'LOADING' : null}</div>
                {this.renderTodoList()}
            </div>
        );
    }
}

const mapStateToProps = ({ todos }: StoreState): { todos: Todo[] } => {
    return { todos };
};

const mapDispatchToProps = { fetchTodos, deleteTodo };

export const App = connect(mapStateToProps, mapDispatchToProps)(_App);
